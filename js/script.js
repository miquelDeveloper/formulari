// numero d'articles minim
var min_articles = 2;

window.onload = function () {
    //var panel = document.getElementsByClassName('cntArticle');
};

function afegirArticle() {

    let comptador = document.getElementById('comptador_articles');
    let btnEnviar = document.getElementById('submitButton');
    let value = parseInt(comptador.textContent);


    if (value) {
        //console.log(`articles: ${value}`);
        // guardat de l'element tope
        let destiNouElement = document.getElementsByClassName('cntBotons')[0];
        //console.log(destiNouElement);
        // recuperem els elements actuals al formulari
        let panel = document.getElementsByClassName('cntArticle');
        // recuperem el desti on afegirem el nou element
        let desti = panel[0].parentNode;

        // inicialitzem contadors
        let identLastElement = panel.length - 1;
        let identNextElement = identLastElement + 1;
        // clonem l'últim element
        let newElement = panel[identLastElement].cloneNode(true);
        // personalitzem els index de l'element
        customizeArticle(newElement, identNextElement, 'id_article');
        customizeArticle(newElement, identNextElement, 'id_producte');
        customizeArticle(newElement, identNextElement, 'numero_productes');
        customizeArticle(newElement, identNextElement, 'preu_unitat');
        //customizeArticle(newElement, identNextElement, 'data_venta');

        customizeArticle(newElement, identNextElement, 'btnBorrarArticle');

        customizeArticleId(newElement, identNextElement, 'btnBorrarArticle', 'btnBorrarArticle-');
        newElement.id = 'cntArticle-' + identNextElement;
        //customizeArticleId(newElement, identNextElement, 'cntArticle', 'cntArticle-');
        // afegim l'element al formulari
        desti.insertBefore(newElement, destiNouElement);

        // Situem el focus del formulari
        document.getElementsByClassName('id_producte')[comptador.textContent].value='';
        document.getElementsByClassName('id_producte')[comptador.textContent].focus();

        // actualitzem comptador articles
        comptador.textContent = value + 1;

        btnEnviar.disabled = comptador.textContent >= min_articles ? false : true;


    } else {
        comptador.textContent = `${value} Numero màxim d'articles`;
    }

}

function borrarArticle(id) {
    if (confirm("Borrar el articulo de la lista ?")) {
        let btnEnviar = document.getElementById('submitButton');
        let idArticle = id.split('-', 2)[1];
        let articleSeleccionat = idArticle ? document.getElementById(`cntArticle-${idArticle}`) : null;
        let status = articleSeleccionat.remove();
        let comptador = document.getElementById('comptador_articles');
        let value = parseInt(comptador.textContent);
        comptador.textContent = value - 1;

        btnEnviar.disabled = comptador.textContent < min_articles ? true : false;
        //newElement.getElementsByClassName(element)[idArticle]
//        console.log(articleSeleccionat);
//        console.log(id);
    }
}

function getNomCamp(nom) {

    let indexLast = nom.lastIndexOf(']');
    let indexFirst = nom.lastIndexOf('[') + 1;
    let nomCamp = nom.substring(indexFirst, indexLast);

    return nomCamp;

}

function customizeArticleId(newElement, newIndex, element, id) {
    let idItem = newElement.getElementsByClassName(element)[0];

    //let getIdCamp = idItem.id;
    idItem.id = id + newIndex;



}

function customizeArticle(newElement, newIndex, element) {
    //console.log(newElement);

    let idItem = newElement.getElementsByClassName(element)[0];
    //console.log(newElement);

    // recuperem el nom de la propietat del camp
    let getNameCamp = getNomCamp(idItem.name);

    if (element === 'btnBorrarArticle') {
        let btnBorrar = newElement.getElementsByClassName(element)[0];
        btnBorrar.style.display = 'flex';
        /*btnBorrar.id = newIndex;*/
    }


    // definicio nous valors
    let valueNom = `articles[${newIndex}][${getNameCamp}]`;
    // assignem
    idItem.name = valueNom;
    idItem.value = element == 'id_article' ? newIndex : 0;


}

