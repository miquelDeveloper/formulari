<?php

include 'model/GestioComandesModel.php';
include 'libs/config.php';

/**
 * Description of gestioComandes
 *
 * @author miquel regidor
 */
class GestioComandesController {

    protected $article = null;
    protected $articles = [];
    protected $form = null;
    public $errorForm = null;
    protected $path_form = null;
    protected $numArticlesFormulari = 1; // valor inicial del formulari,(Comptador de articles)
    protected $idClient = null;
    protected $model = null;
    protected $estoc = null;

    public function __construct() {
        $this->idClient = null;
        $this->path_form = htmlspecialchars($_SERVER["PHP_SELF"]);
        $this->model = GestioComandesModel::getInstance();
    }

    /**
     * Comprovació del format de la data per seguretat
     * @param type $data
     * @return bool
     */
    function checkData($data): bool {
        $format = 'Y-m-d';
        $dataAux = filter_var(htmlspecialchars($data), FILTER_SANITIZE_STRING);
        $data = DateTime::createFromFormat($format, $dataAux);
        if ($data) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Funcio auxiliar per el tractament dels decimals, per usabilitat l'usuari pot fer servir tan punts(.) com comes(,) a l'hora
     * d'entrar les dades en els camps de unitats i preu.
     * @param string $value
     *
     */
    private function corretgeixComes(string $value) {
        $str = str_replace(',', '.', $value);
        $value = doubleval($str);
        return $value;
    }

    /**
     * Tipat de dades, definicio del tipus de dades dessitjat per la persistencia de dades.
     * @param type $item
     * @return type
     */
    private function tipatDades($item) {
        $item['preu_unitat'] = doubleval($this->corretgeixComes($item['preu_unitat']));
        $item['numero_productes'] = $this->corretgeixComes($item['numero_productes']);
        $item['id_producte'] = intval(htmlspecialchars($item['id_producte']));
        $item['preu_unitat'] = doubleval(htmlspecialchars($item['preu_unitat']));
        $item['numero_productes'] = doubleval(htmlspecialchars($item['numero_productes']));
        return $item;
    }

    /**
     * Comprovació de les dades rebudes per seguretat.
     * @param type $post
     * @return boolean
     */
    private function checkValuePost($post = null): bool {
        $result = false;
        if ($post) {
            foreach ($post['articles'] as $key => $article) {
                $itemTipat = $this->tipatDades($article);
                if ($itemTipat) {
                    $result = filter_var($itemTipat['numero_productes'], FILTER_VALIDATE_FLOAT) && $result ? true : false;
                    $result = filter_var($itemTipat['id_producte'], FILTER_VALIDATE_INT) ? true : false;
                    $result = filter_var($itemTipat['preu_unitat'], FILTER_VALIDATE_FLOAT) && $result ? true : false;
                    $result ? $this->altaArticle($itemTipat) : null;
                } else {
                    return false;
                }
            }
        }
        return $result;
    }

    /**
     * Validació de les dades POST
     * @param type $post
     * @return type
     */
    private function checkPost($post = null) {
        $result = false;
        // comprovació número d'articles
        if ($post && count($post['articles']) >= ARTICLES_MINIM_COMANDA ) {
            $this->idClient = intval($post['rid_client']);
            $this->checkData($post['data_venta']) ? $this->dataVenta = $post['data_venta'] : null;
            if ($this->idClient && $this->dataVenta) {
                $result = $this->checkValuePost($post) ? true : false;
            }
            return $result;
        } else {
            $this->errorForm = "Número d'articles invalid";
            return $result;
        }
    }

    /**
     * Control de l'accés al formulari i gestio de la validació de dades.
     * @param type $post
     */
    public function controlAcces($post = null) {
        if ($this->model->getConn() != null) {
            if ($post && isset($post['formulari_compra']) && isset($post['data_venta'])) {

                if ($this->checkPost($post)) {
                    $this->saveData();
                    header('Location: confirmacio.html');
                } else {
                    $this->errorForm === '' ? $this->errorForm = "error al guardar les dades" : null;
                    $this->render();
                }
            } else {
                $this->render();
            }
        } else {
            $this->errorForm = "Error d'accés a base de dades";
            $this->render();
        }
    }

    /**
     * Renderitzat de formulari HTML
     */
    private function render() {
        $this->setForm();
        echo $this->form;
    }

    /**
     * Creacio del formulari HTML
     */
    private function setForm() {
        $this->form = "<div class='cntForm'>
    <div class='titulForm'>Formulari d'entrada de moviments </div>
    <div class='interaccioUsuari'>$this->errorForm</div>
    <form action='$this->path_form' method='POST' class='formulari_venta_article'>
        <input type='hidden' name='formulari_compra'>
        <input type='hidden' name='rid_client' value='10'>
        <div class='cntArticle' id='0'>
                <input type='hidden' class='id_article' name='articles[0][id]'>
                <div class='firstLine'>
                    <label type='label' for='id_producte'>Codi Producte</label>
                    <input type='number' class='id_producte' name='articles[0][id_producte]' min='1' required='true' value=''>
                    <label type='label' for='numero_productes'>Quantitat</label>
                    <input type='number' class='numero_productes' step='0.01' name='articles[0][numero_productes]' value='0' required='true'>
                    <label type='label' for='preu_unitat'>Preu Unitat</label>
                    <input type='number' class='preu_unitat' step='0.01' name='articles[0][preu_unitat]' value='0' required='true'>
                </div>
                <button id='' onClick='borrarArticle(this.id)' class='btnBorrarArticle' type='button'>X</button>
                </div>
                <div class='cntBotons'>
                <div class='butonsLine'>
                    <label type='label' for='data_venta'>Data Venta</label>
                    <input type='date' class='data_venta' name='data_venta' required='true'>
                    <input type='button' name='btn_afegir_article' data-toggle='tooltip' onClick='afegirArticle()' class='btn_add' value='Afegir Article'>
                    <input type='submit' id='submitButton' disabled='false' class='submitButton' name='btn' value='Enviar comanda'>
                </div>
                <div class='infoLine'>
                    <p>Número d'articles: 0<span id='comptador_articles'>$this->numArticlesFormulari</span></p>
                    <p>Decimals(,)  Mínim ";
        $this->form .= ARTICLES_MINIM_COMANDA;
        $this->form .= " Articles</p>
                </div>
                </div>
            </form>

        </div>";
    }

    /**
     * Conversio de array article a stdClass     *
     * @param type $article
     */
    private function altaArticle($article) {
        $this->article = new stdClass;
        $this->article->codiProducte = $article['id_producte'];
        $this->article->preuUnitat = $article['preu_unitat'];
        $this->article->numeroProductes = $article['numero_productes'];
        $this->articles[] = $this->article;
    }

    /**
     * S'encarrega d'actualitzar l'estoc segons el moviment
     * @param type $moviment
     * @param type $article
     */
    private function actualitzaEstoc($moviment, $article) {
        $articleExistent = $this->model->getDetallProducte($article->codiProducte, 'id_producte');
        switch ($moviment) {
            case 'V':
                $this->stoc = $articleExistent['stock_act'] - $article->numeroProductes;
                break;
            case 'D':
                $this->stoc = $articleExistent['stock_act'] + ($article->numeroProductes * -1);
                break;
                defaut:
                break;
        }
    }

    /**
     * Actualitzar un article determinat
     * @param type $idArticle
     * @return type
     */
    private function actualitzarArticle($idArticle) {
        $result = false;
        $result = $this->model->getDetallProducte($idArticle, 'id_producte');
        return $result;
    }

    /**
     * Guarda linies i actualitza els articles en cas de ser necessari
     * @param type $idAlbara
     * @param type $articlesAlbara
     */
    private function guardaLiniaAlbara($idAlbara, $articlesAlbara) {

        foreach ($articlesAlbara as $article) {

            // determinem moviment de productes
            $moviment = $article->numeroProductes < 0 ? 'D' : 'V';

            if ($this->actualitzarArticle($article->codiProducte)) {
                //actualitzem estoc
                $this->actualitzaEstoc($moviment, $article);
                $this->model->actualitzarArticle($this->stoc, $article->codiProducte, $article->preuUnitat);
                $idArticle = $article->codiProducte;
            } else {
                //article nou, inicialitzem l'estoc
                $this->stoc = 0 - $article->numeroProductes;
                $idArticle = $this->model->crearArticle($article->codiProducte, $article->preuUnitat, $this->stoc);
            }

            // Crea linea de detall de l'albarà
            $this->model->crearDetallAlbara(
                    intval($idAlbara), intval($idArticle), $article->numeroProductes, $article->preuUnitat, $moviment
            );
        }
    }

    /**
     * Gravacio de les dades a la BD creant els registres necessaris.
     * Creacio de un registre a les taules: albara, albara_detall, producte
     * Actualització de les dades en cas d'entrar un article existent
     */
    public function saveData() {

        if ($this->idClient && $this->dataVenta) {
            $idAlbara = $this->model->crearAlbara($this->dataVenta, intval($this->idClient));
            $this->guardaLiniaAlbara($idAlbara, $this->articles);
        }
    }

}
