<?php
/* Fitxer amb les dades de configuracio generals del formulari */

/* Dades de connexió a BD */
const HOST = 'localhost';
const USER = 'provaUser';
const PASSWORD = 'prova1001';
const DB = 'prova';
const CHARSET = 'utf8';
/* Dades de connexió a BD */

/** dades del funcionament del formulari */
const ARTICLES_MINIM_COMANDA = 2; // numero d'articles minim per fer la comanda. ( Mirar també js/script.js )
//const ARTICLES_MAXIM_COMANDA = 1000; // Es podria configurar un limit d'articles per enviament, deshabilitat inicialment.
/** dades del funcionament del formulari */

/* Eliminar abans de empaquetar - desenvolupament */
error_reporting(E_ALL);
ini_set('display_errors', '1');
