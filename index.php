<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>ProvaPHP</title>
        <script src="js/script.js"></script>
        <link rel="stylesheet" href="css/estils.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
    <?php
    include 'controller/GestioComandesController.php';
    $gestioComandes = new GestioComandesController();
    $gestioComandes->controlAcces($_POST);
    ?>
    </body>
</html>