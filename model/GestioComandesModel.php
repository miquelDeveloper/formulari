<?php
include 'libs/Database.php';
/*
 * Model per la gestió de la persistencia de dades
 * Amb les dades del fitxer libs/config configura la connexió a BD i proporciona un objecte PDO
 */
/**
 * Model de dades GestioComandesModel
 * @author miquel regidor
 */
class GestioComandesModel {

    private $db = null; // gestio bd
    private $link = null; // gestiona la connexió
    private static $instance = null;

    private function __construct() {
        $this->db = new Database();
        $this->link = $this->db->connect();
        unset($this->db);
    }

    /**
     * Proporciona una instancia
     * @return type
     */
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new GestioComandesModel();
        }

        return self::$instance;
    }

    /**
     * Proporciona una connexio PDO
     * @return type
     */
    public function getConn() {
        return $this->link;
    }

    /**
     * Gestiona la persistencia de dades en Base de dades dels articles.
     * @param type $articles
     * @return boolean
     */
    public function saveData($articles) {
        try {
            //Realizamos la consulta
            $query = $this->link->connect()->prepare(
                    "INSERT INTO `albara` (`id_venda`, `rid_client`, `dia`,"
                    . "VALUES (:id_venda, :rid_client, :dia)");

            $query->execute([
                'id_venda' => $idCarro,
                'rid_client' => $dadesComprador->FirstName,
                'dia' => $dadesComprador->LastName
            ]);
        } catch (PDOException $eGEN) {
            return false; // retornamos array vacio en caso de error
        }
    }

    /**
     * Obté un detall de un producte de la base de dades
     * @param type $article
     * @param type $detall
     * @return type
     */
    public function getDetallProducte($article, $detall) {

        try {

            $sql = "SELECT * FROM producte WHERE id_producte=:article";
            $query = $this->link->prepare($sql);
            $query->execute(['article' => $article]);
            $value = $query->fetch();

            return $value;
        } catch (PDOException $eGEN) {
            return [$eGEN]; // retornamos array vacio en caso de error
        }
    }

    /**
     * Crea el registre necessari per la creació d'un albarà
     * @param type $data
     * @param type $id_client
     * @return boolean
     */
    public function crearAlbara($data, $id_client) {
        try {
            $sql = "INSERT INTO albara (rid_client, dia) VALUES (:rid_client, :dia)";
            $query = $this->link->prepare($sql);

            $query->execute([
                'rid_client' => $id_client,
                'dia' => $data
            ]);
            return $this->link->lastInsertId();
        } catch (PDOException $eGEN) {
            return false; // retornamos array vacio en caso de error
        }
    }

    /**
     * Crea el registre necessari per la creació d'un article
     * @param type $id
     * @param type $preu
     * @param type $estoc
     * @return boolean
     */
    public function crearArticle($id, $preu, $estoc) {
        try {
            $sql = "INSERT INTO producte (id_producte,preu,stock_act) VALUES (:id, :preu,:stock_act)";
            $query = $this->link->prepare($sql);

            $query->execute([
                'id' => $id,
                'preu' => $preu,
                'stock_act' => $estoc
            ]);
            return $this->link->lastInsertId();
        } catch (PDOException $eGEN) {
            return false;
        }
    }

    /**
     *  Actualitza les dades d'un article
     * @param type $estoc
     * @param type $article
     * @param type $preu
     * @return boolean
     */
    public function actualitzarArticle($estoc, $article, $preu) {
        try {
            $sql = "UPDATE producte SET stock_act = :stock_act, preu = :preu WHERE id_producte = :id ";
            $query = $this->link->prepare($sql);

            $query->execute([
                'stock_act' => $estoc,
                'preu' => $preu,
                'id' => $article
            ]);
        } catch (PDOException $eGEN) {
            return false;
        }
    }

    /**
     * Crea el registre necessari per la creació d'un detall de l'albara
     * @param type $idAlbara
     * @param type $codiProducte
     * @param type $unitats
     * @param type $preuUnitari
     * @param type $moviment
     * @return boolean
     */
    public function crearDetallAlbara($idAlbara, $codiProducte, $unitats, $preuUnitari, $moviment) {
        try {
            $sql = "INSERT INTO albara_detall (rid_venda, rid_prod, unitats, preu_unitari, tipus_mov) "
                    . "VALUES (:rid_venda, :rid_prod, :unitats, :preu_unitari, :tipus_mov)";
            $query = $this->link->prepare($sql);
            $query->execute([
                'rid_venda' => $idAlbara,
                'rid_prod' => $codiProducte,
                'unitats' => $unitats,
                'preu_unitari' => $preuUnitari,
                'tipus_mov' => $moviment
            ]);
        } catch (PDOException $eGEN) {
            return $eGEN;
        }
    }

}